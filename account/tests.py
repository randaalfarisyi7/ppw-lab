from django.contrib.auth.models import User
from django.test import TestCase

class LogInTest(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'user',
            'password': 'secret'}
        User.objects.create_user(**self.credentials)
    def test_login(self):
        # send login data
        response = self.client.post('/login', self.credentials, follow=True)
        # should be logged in now
        self.assertTrue(response.context['user'].is_authenticated)

        response2 = self.client.get('/login')
        self.assertEquals(response2.status_code, 302)

        response3 = self.client.get('/')
        self.assertEquals(response3.status_code, 200)

    def test_logout(self):
    	self.client.login(username='XXX', password="XXX")
    	# Check response code
    	response = self.client.get('/login')
    	self.assertEquals(response.status_code, 200)
    	# Log out
    	self.client.logout()
    	response = self.client.get('/')
    	self.assertEquals(response.status_code, 200)

    def test_signup(self):
    	response = self.client.post('/register', {'usernama'	: 'randaal', 'email':'randaal@gmail.com', 'password1':'secret123', 'password2':'secret123'})
    	self.assertEquals(response.status_code, 200)