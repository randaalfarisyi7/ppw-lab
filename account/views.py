from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from .forms import CreateUserForm

# Create your views here.
def registerPage(request):
	if request.user.is_authenticated:
		return HttpResponseRedirect('/')
	else:
		form = CreateUserForm()
		if request.method == "POST":
			form = CreateUserForm(request.POST)
			if form.is_valid():
				form.save()
				user = form.cleaned_data.get('username')
				messages.success(request, 'Account was created for ' + user)
				return HttpResponseRedirect('/login')

	context = {'form' : form}
	return render(request, 'main/register.html', context)

def loginPage(request):
	if request.user.is_authenticated:
		return HttpResponseRedirect('/')

	else:
		if request.method == "POST":
			username = request.POST.get('username')
			password = request.POST.get('password')

			user = authenticate(request, username=username, password=password)
			request.session['user_login'] = username
			if user is not None:
				login(request, user)


				return HttpResponseRedirect('/')

			else:
				messages.info(request, 'Username OR password is incorrect')


	context = {}
	return render(request, 'main/login.html', context)

def logoutUser(request):
	print (request.session['user_login'])
	request.session.flush()
	logout(request)
	return HttpResponseRedirect('/')
