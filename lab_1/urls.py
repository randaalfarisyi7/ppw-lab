"""lab_1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.contrib import admin
from django.urls import re_path, path
from app_1.views import index as index_lab1
from app_1.views import index_1 as index_lab3
from lab_6.views import form1 as form1
from lab_6.views import saveForm1
from lab_6.views import saveForm2
from lab_6.views import form2 as form2
from lab_6.views import daftarKegiatan as daftarKegiatan



urlpatterns = [
    re_path('admin/', admin.site.urls),
    re_path(r'^lab_1', index_lab1, name='index'),
    re_path(r'^halaman', index_lab3, name='index'),
    re_path('', include('lab_4.urls')),
    re_path(r'^formulir1', form1, name='form1'),
    re_path('saveForm1', saveForm1),
    re_path(r'^formulir2', form2, name='form2'),
    re_path('saveForm2', saveForm2),
    re_path(r'^daftarKegiatan', daftarKegiatan, name='daftarKegiatan'),
    re_path('', include('lab_8.urls')),
    re_path('', include('account.urls')), 
]
