from django import forms
from .models import MataKuliah

class Input_Form(forms.ModelForm):
	class Meta:
		model = MataKuliah
		fields = ['display_name','dosen','sks', 'deskripsi', 'semester_tahun', 'ruang_kelas']
		error_messages = {
			'required' : 'Please Type'
		}
	input_attrs = {
		'type' : 'text',
		'placeholder' : 'Nama Matkul'
	}
	sks_input = {
		'type' : 'text',
		'placeholder' : 'Jumlah SKS'
	}
	dosen_input = {
		'type' : 'text',
		'placeholder' : 'Nama Dosen'
	}
	deskripsi_input = {
		'type' : 'text',
		'placeholder' : 'Deskripsi'
	}
	semester_input = {
		'type' : 'text',
		'placeholder' : 'Semester dan Tahun Ajaran'
	}
	ruang_input = {
		'type' : 'text',
		'placeholder' : 'Ruang Kelas'
	}

	display_name = forms.CharField(label='', required=False, max_length=60, widget=forms.TextInput(attrs=input_attrs))
	dosen = forms.CharField(label='', required=False, max_length=60, widget=forms.TextInput(attrs=dosen_input))
	sks = forms.CharField(label='', required=False, max_length=6, widget=forms.TextInput(attrs=sks_input))
	deskripsi = forms.CharField(label='', required=False, max_length=200, widget=forms.TextInput(attrs=deskripsi_input))
	semester_tahun = forms.CharField(label='', required=False, max_length=30, widget=forms.TextInput(attrs=semester_input))
	ruang_kelas = forms.CharField(label='', required=False, max_length=30, widget=forms.TextInput(attrs=ruang_input))



