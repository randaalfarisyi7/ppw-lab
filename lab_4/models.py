from django.db import models
from django.utils import timezone
from datetime import datetime, date

class MataKuliah(models.Model):
    display_name = models.CharField(max_length=60)
    dosen = models.CharField(max_length=60)
    sks = models.CharField(max_length=6)
    deskripsi = models.CharField(max_length=200)
    semester_tahun = models.CharField(max_length=30)
    ruang_kelas = models.CharField(max_length=30)

# Create your models here.
