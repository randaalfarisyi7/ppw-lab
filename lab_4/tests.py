from django.test import TestCase, Client


class TestingLab4(TestCase):

	def test_apakah_url_home_ada(self):
		response = Client().get('/')
		self.assertEquals(response.status_code, 200)

	def test_apakah_home_ada_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'main/home.html')


	def test_apakah_url_pendidikan_ada(self):
		response = Client().get('/pendidikan/')
		self.assertEquals(response.status_code, 200)

	def test_apakah_pendidikan_ada_template(self):
		response = Client().get('/pendidikan/')
		self.assertTemplateUsed(response, 'main/pendidikan.html')

	def test_apakah_url_matkul_ada(self):
		response = Client().get('/matkul/')
		self.assertEquals(response.status_code, 200)

	def test_apakah_matkul_ada_template(self):
		response = Client().get('/matkul/')
		self.assertTemplateUsed(response, 'main/matkul.html')

	def test_apakah_url_readmatkul_ada(self):
		response = Client().get('/readmatkul/')
		self.assertEquals(response.status_code, 200)

	def test_apakah_readmatkul_ada_template(self):
		response = Client().get('/readmatkul/')
		self.assertTemplateUsed(response, 'main/readmatkul.html')