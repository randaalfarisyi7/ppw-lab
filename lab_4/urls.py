from django.urls import re_path

from . import views

app_name = 'lab_4'

urlpatterns = [
    re_path(r'^$', views.home, name='home'),
    re_path(r'^pendidikan', views.pendidikan, name='pendidikan'),
    re_path(r'^matkul', views.matkul, name='matkul'),
    re_path('savematkul', views.savematkul),
    re_path('readmatkul', views.readmatkul, name='readmatkul'),
    re_path(r'^delete/(?P<data>[-\w]+)/$',views.delete, ),
    re_path(r'^detail/(?P<data>[-\w]+)/$', views.readmatkulDetail, ),
]