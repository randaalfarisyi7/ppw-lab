from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Input_Form
from .models import MataKuliah
from django.contrib.auth.decorators import login_required


def home(request):
	if request.user.is_authenticated:
		user = request.session['user_login']
		response = {'author' : user}
	else:
		response = {'author' : ' '}
	return render(request, 'main/home.html', response)


# @login_required(login_url='/login')
def pendidikan(request):
    return render(request, 'main/pendidikan.html')


# @login_required(login_url='/login')
def matkul(request):
	if request.user.is_authenticated:
		user = request.session['user_login']
		response = {'author' : user, 'input_form' : Input_Form}
	else:
		response = {'author' : ' '}
	return render(request, 'main/matkul.html', response)

# @login_required(login_url='/login')
def savematkul(request):
	if request.method == 'POST':
		form = Input_Form(request.POST)
		if form.is_valid():
		
				form.save()
				return HttpResponseRedirect('/readmatkul')
		else:
				return HttpResponseRedirect('/matkul')
	else:
		return HttpResponseRedirect('/matkul')

# @login_required(login_url='/login')
def readmatkul(request):
	matkuls = MataKuliah.objects.all()
	if request.user.is_authenticated:
		user = request.session['user_login']
		response = {'author' : user, 'matkuls' : matkuls}
	else:
		response = {'author' : ' '}
	return render(request, 'main/readmatkul.html', response)

# @login_required(login_url='/login')
def delete(request, data):
	emp = MataKuliah.objects.get(display_name = data)
	emp.delete()
	return HttpResponseRedirect('/readmatkul')

# @login_required(login_url='/login')
def readmatkulDetail(request, data):
	matkul = MataKuliah.objects.get(display_name = data)
	if request.user.is_authenticated:
		user = request.session['user_login']
		response = {'author' : user, 'matkul' : matkul}
	else:
		response = {'author' : ' '}
	

	return render(request, 'main/readmatkulDetail.html', response)

