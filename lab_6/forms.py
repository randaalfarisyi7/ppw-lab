from django import forms
from .models import Formulir_pertama, Formulir_kedua

class Input_Form(forms.ModelForm):
	class Meta:
		model = Formulir_pertama
		fields = ['name','email']
		error_messages = {
			'required' : 'Please Type'
		}
	name = forms.CharField(label='', required=False, max_length=60)
	email = forms.CharField(label='', required=False, max_length=60)


class Input_Form2(forms.ModelForm):
	class Meta:
		model = Formulir_kedua
		fields = ['name','email']
		error_messages = {
			'required' : 'Please Type'
		}
	name = forms.CharField(label='', required=False, max_length=60)
	email = forms.CharField(label='', required=False, max_length=60)


	


