from django.test import TestCase, Client
from .models import Formulir_pertama, Formulir_kedua
from .forms import Input_Form, Input_Form2

# Create your tests here.
class TestingLab6(TestCase):

	def test_apakah_url_form_1_ada(self):
		response = Client().get('/formulir1/')
		self.assertEquals(response.status_code, 200)

	def test_apakah_form1_ada_templatenya(self):
		response = Client().get('/formulir1/')
		self.assertTemplateUsed(response, 'main/formulir1.html')

	def test_apakah_sudah_ada_model_form1(self):
		Formulir_pertama.objects.create(name="Ini Nama", email="ini email")
		hitung_banyak_peserta = Formulir_pertama.objects.all().count()

	def test_apakah_sudah_ada_model_form2(self):
		Formulir_kedua.objects.create(name="Nama nih", email="email nih")
		hitung_banyak_peserta = Formulir_kedua.objects.all().count()

	def test_apakah_di_halaman_daftarKegiatan_sudah_menyimpan_data_dan_ditampilkan(self):
		response = Client().post('/saveForm1/', {'nama'	: 'Ini Nama', 'email':'ini email'})
		self.assertEquals(response.status_code, 302)

		response_2 = Client().post('/saveForm2/', {'nama'	: 'Nama nih', 'email':'email nih'})
		self.assertEquals(response_2.status_code, 302)
		self.assertEqual(response['location'], '/daftarKegiatan')


	def test_apakah_url_form_2_ada(self):
		response = Client().get('/formulir2/')
		self.assertEquals(response.status_code, 200)

	def test_apakah_form2_ada_templatenya(self):
		response = Client().get('/formulir2/')
		self.assertTemplateUsed(response, 'main/formulir2.html')


	def test_apakah_url_daftar_Kegiatan_ada(self):
		response = Client().get('/daftarKegiatan/')
		self.assertEquals(response.status_code, 200)

	def test_apakah_daftarKegiatan_ada_templatenya(self):
		response = Client().get('/daftarKegiatan/')
		html_kembalian = response.content.decode('utf8')
		self.assertIn("Nama", html_kembalian)
		self.assertTemplateUsed(response, 'main/daftarKegiatan.html')

	def test_form(self):
		form = Input_Form()
		self.assertIn('id="id_name"', form.as_p())
		self.assertIn('id="id_email', form.as_p())

	def test_form2(self):
		form2 = Input_Form2()
		self.assertIn('id="id_name"', form2.as_p())
		self.assertIn('id="id_email', form2.as_p())

	def test_form_validation_for_blank_items(self):
		form = Input_Form(data={'name': '', 'email':''})
		form.save()
