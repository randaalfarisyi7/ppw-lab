from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Input_Form, Input_Form2
from .models import Formulir_pertama, Formulir_kedua
from django.contrib.auth.decorators import login_required

# Create your views here.
# @login_required(login_url='/login')
def form1(request):
	if request.user.is_authenticated:
		user = request.session['user_login']
		response = {'author' : user}
	else:
		response = {'author' : ' '}
	return render(request, 'main/formulir1.html', response)

# @login_required(login_url='/login')
def saveForm1(request):
	form = Input_Form(request.POST)

	if form.is_valid():
		print("bisa")
		if request.method == 'POST':
				form.save()
				return HttpResponseRedirect('/daftarKegiatan')

# @login_required(login_url='/login')
def form2(request):
	if request.user.is_authenticated:
		user = request.session['user_login']
		response = {'author' : user}
	else:
		response = {'author' : ' '}
	return render(request, 'main/formulir2.html', response)

# @login_required(login_url='/login')
def saveForm2(request):
	form = Input_Form2(request.POST)
	if form.is_valid():
		if request.method == 'POST':
				form.save()
				return HttpResponseRedirect('/daftarKegiatan')

# @login_required(login_url='/login')
def daftarKegiatan(request):
	form1 = Formulir_pertama.objects.all()
	form2 = Formulir_kedua.objects.all()
	if request.user.is_authenticated:
		user = request.session['user_login']
		response = {'author' : user, 'forms1' : form1, 'forms2' : form2}
	else:
		response = {'author' : ' '}
	return render(request, 'main/daftarKegiatan.html', response)