from django.test import TestCase, Client

# Create your tests here.
class TestingLab8(TestCase):
	"""docstring for testingLab"""
	def test_apakah_url_buku_ada(self):
		response = Client().get('/buku/')
		self.assertEquals(response.status_code, 200)

	def test_apakah_buku_ada_templatenya(self):
		response = Client().get('/buku/')
		self.assertTemplateUsed(response, 'main/listBuku.html')


	def test_apakah_url_data_ada_dengan_buku_yang_dicari(self):
		response = Client().get('/data?q=harry')
		self.assertEquals(response.status_code, 200)
