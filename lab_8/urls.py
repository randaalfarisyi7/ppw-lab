from django.urls import re_path
from . import views

app_name = 'lab_8'

urlpatterns = [
    re_path(r'^buku', views.buku, name='buku'),
    re_path('data', views.daftar_buku),
]