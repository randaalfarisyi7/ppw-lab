from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
import requests
import json
from django.contrib.auth.decorators import login_required

# Create your views here.
# @login_required(login_url='/login')
def buku(request):
	if request.user.is_authenticated:
		user = request.session['user_login']
		response = {'author' : user}
	else:
		response = {'author' : ' '}
	return render(request, 'main/listBuku.html', response)

def daftar_buku(request):
	url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
	ret = requests.get(url)
	data = json.loads(ret.content)
	return JsonResponse(data, safe=False)
